# README #

Proyecto para mostrar distintos patrones de codificación de javascript.

# Conceptos #

* Alcance de variables
* hoisting
* single var
* creacion de funciones
* module
* reveal

# Fuente #
El proyecto fue creado inspirado en:

* [http://shichuan.github.io/javascript-patterns/](http://shichuan.github.io/javascript-patterns/)
* [http://addyosmani.com/resources/essentialjsdesignpatterns/book/](http://addyosmani.com/resources/essentialjsdesignpatterns/book/)
* [http://www.w3schools.com/js/js_function_closures.asp] (http://www.w3schools.com/js/js_function_closures.asp)
